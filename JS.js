
//Tar inn alle de elementene med id fra html
// De under er mer "konstante" variabler

// Under select-elementer
const laptopsElement = document.getElementById("laptops"); 

// Knapper
const workElement = document.getElementById("work"); 
const bankElement = document.getElementById("bank");
const loanElement = document.getElementById("loan")
const repayloanElement = document.getElementById("repayloan")


// "Attributtene" til computer
const priceElement = document.getElementById("price"); 
const featuresElement = document.getElementById("features")
const titleElement = document.getElementById("title");
const infoElement = document.getElementById("info");

// Overføring av penger osv
const payElement = document.getElementById("pay")
const balanceElement = document.getElementById("balance"); 

// Lån
const banksectionElement = document.getElementById("banksection"); 
const loanTextElement = document.getElementById("loanbalance");

// Betale
const buynowElement = document.getElementById("buynow"); 

// Repay button hidden
// const repayloanItem = document.createElement("button"); 
// repayloanItem.style.display = "none"; 
// repayloanItem.innerText = `Repay loan`; 
// banksectionElement.appendChild(repayloanItem); 

// Alle PC`ene fra API`et
let laptops = []; 
// Det man har fått i lønn hittil
let pay = 0.0; 
// Det som er i banken
let bank = 0.0; 
// Det man har i lån
let loan = 0.0; 
// Antall lån man har fått kan ikke være større enn 
// 1 før man kjøper en PC
let loancount = 0; 

// Lån eller ikke lån
// let loancount = new Boolean(false);


// Under henter vi fra API, en string, hvor vi setter at variabelen
// "laptops" = data på et vis
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops)); 

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x)); 
    priceElement.innerText = `Price:  ${laptops[0].price} kr`; 
    titleElement.innerText = laptops[0].title; 
    infoElement.innerText = laptops[0].specs; 
    featuresElement.innerText = laptops[0].description;
    // Over: Tar inn nummer 0 som default pris som vises med en gang.
 }

 // Under: Legger til laptops i select-elementet til HTML
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option"); 
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title)); 
    laptopsElement.appendChild(laptopElement); 
}

const handleComputerMenuChange = e => {
    const selectedlaptop = laptops[e.target.selectedIndex]; 
    priceElement.innerText = `Price:  ${selectedlaptop.price} kr` ;
    featuresElement.innerText = selectedlaptop.description;
    titleElement.innerText = selectedlaptop.title; 
    infoElement.innerText = selectedlaptop.specs; 
}


const addWorkMoney = () => {
    pay += 100; 
    payElement.innerText = `You have got ${pay} kr in salary this far`;
    payElement.appendChild(pay); 
}

const addBankMoney = () => {
    
    if (loancount == 0) {
        bank += pay; 
        pay = 0.0; 
        payElement.innerText = `You have 0 in salary this far`;
        balanceElement.innerText = `You have ${bank} kr in the bank`;
        bankElement.appendChild(bank); 
    } 
    
    if (loan > 0)  {
 
        bank = bank + (0.9 * pay); 
        balanceElement.innerText = `You have ${bank} kr in the bank`;

        if ((0.1 * pay) <= loan) {
            loan = loan - (0.1 * pay);
            loanTextElement.innerText = `You have ${loan} kr in loan`;
        } else {

            bank += ((0.1 * pay) - loan); 
            loan = 0.0; 
            loancount = 0; 
            balanceElement.innerText = `You have ${bank} kr in the bank`;
            loanTextElement.innerText = `You have ${loan} kr in loan`;
        }

        pay = 0.0; 
        payElement.innerText = `You have 0 in salary`;
    }

}

const getALoan = () => {
    const loanConst = parseInt(prompt("Please enter the amount of money you want to loan ")); 

    if (Number(loanConst < (bank*2))) {
            if (loancount == 0) {
                bank += Number(loanConst); 
                loancount = 1; 
                balanceElement.innerText = `You have got ${bank} kr in the bank`;

                loan = loanConst; 
                loanTextElement.innerText = `You have ${loan} kr in loan`; 
                
                repayloanElement.className = 'repayloan-button'

                // repayloanElement.style.visibility = "visible";
                // repayloanItem.style.display ="block"; 
                // document.getElementById("repayloan").style.display = "none"; 

            } else {
                alert(`You are already loaning money. Pay that back first`); 
            }

    } else { alert("Sorry, that is way too much"); 
    }
 
}


const repayLoan = () => {
   
    if (loancount == 1) {
        if (pay >= loan){
            pay = pay - loan; 
            loan = 0.0; 
            loancount = 0; 
            payElement.innerText = `You have got ${pay} kr in salary this far`;
            loanTextElement.innerText = `You have ${loan} kr in loan`;
            balanceElement.innerText = `You have got ${bank} kr in the bank`;

            // Fjerner repay-knappen her pga. her så forsvinner jo lånet. 
            repayloanElement.className = 'repayloan-button-hide'

        } else if (pay < loan){
            loan = loan - pay; 
            pay = 0.0; 
            payElement.innerText = `You have got ${pay} kr in salary this far`;
            loanTextElement.innerText = `You have ${loan} kr in loan`;
    }
    
}

}




const buyComputer = () => {


    const selectedlaptop = laptops[laptopsElement.selectedIndex]; 

    if (bank < selectedlaptop.price) {
        alert("You can not afford the laptop"); 
    } else {
        bank -= selectedlaptop.price; 
        loancount = 0; 
        balanceElement.innerText = `You have got ${bank} kr in the bank`;
        alert("You just bought a computer!!")
    }


}


/*
Tanken under: Å sette all oppdatering av tekst og pay/bank/loan i en funksjon, og kalle 
funksjonen for å oppdatere, men fikk den ikke til å funke. 
const updateText = () => {
    balanceElement.innerText = `You have got ${bank} kr in the bank`;
    payElement.innerText = `You have got ${pay} kr in salary this far`;
    loanTextElement.innerText = `You have ${loan} kr in loan`;
}

Bør jeg kanskje skrive dette: 
function functionName() {
  // alle oppdateringene
}
I stedet? Hva er egentlig forskjellen på en funksjon med "const" og en uten "const"? 
*/ 




// EventListeners

laptopsElement.addEventListener("change", handleComputerMenuChange); 

workElement.addEventListener("click", addWorkMoney);

bankElement.addEventListener("click", addBankMoney); 

loanElement.addEventListener("click", getALoan); 

buynowElement.addEventListener("click", buyComputer); 

repayloanElement.addEventListener("click", repayLoan);
